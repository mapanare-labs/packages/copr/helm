%define debug_package %{nil}

Name:           helm
Version:        3.16.3
Release:        1%{?dist}
Summary:        The Kubernetes Package Manager
Group:          Applications/System
License:        ASL 2.0
URL:            https://helm.sh/
Source0:        https://get.helm.sh/%{name}-v%{version}-linux-amd64.tar.gz

%description
The Kubernetes Package Manager

%prep
%setup -qn linux-amd64

%install
install -d -m 755 %{buildroot}%{_bindir}
install -m 755 %{name} %{buildroot}%{_bindir}

%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}

%changelog
* Mon Dec 09 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Tue May 28 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Wed Apr 10 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Sun Mar 03 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 3.14.2

* Thu Dec 28 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 3.13.3

* Wed Nov 15 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 3.13.2

* Thu Aug 31 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 3.12.3

* Wed Jul 26 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 3.12.2

* Mon Jun 19 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 3.12.1

* Thu May 18 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 3.12.0

* Tue Apr 25 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 3.11.3

* Fri Feb 10 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 3.11.1

* Mon Jan 23 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 3.11.0

* Fri Nov 11 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 3.10.2

* Tue Nov 01 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 3.10.1

* Thu Sep 22 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 3.10.0

* Fri Sep 16 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Add README and LICENSE

* Sun Sep 11 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Initial RPM
